/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.0.198
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : 192.168.0.198:3307
 Source Schema         : dangjiang

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 10/06/2022 11:55:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fc_node
-- ----------------------------
DROP TABLE IF EXISTS `fc_node`;
CREATE TABLE `fc_node`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `node_name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点名称',
  `flag` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'system' COMMENT '菜单标识',
  `web_path` varchar(155) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '前端显示路由',
  `node_path` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点路径',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '加载的模板',
  `node_pid` int NULL DEFAULT NULL COMMENT '所属节点',
  `node_icon` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点图标',
  `is_menu` tinyint(1) NULL DEFAULT 1 COMMENT '是否是菜单项 1 不是 2 是',
  `sort` int NULL DEFAULT 0 COMMENT '排序，值越大越靠前',
  `create_time` datetime NULL DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 75 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '节点表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of fc_node
-- ----------------------------
INSERT INTO `fc_node` VALUES (1, '系统管理', 'system', '/sys', '#', '#', 0, 'el-icon-setting', 2, 0, '2021-12-08 15:19:02', NULL);
INSERT INTO `fc_node` VALUES (2, '员工管理', 'system', '/user/index', 'user/index', 'user', 1, '', 2, 0, '2021-12-08 15:20:07', NULL);
INSERT INTO `fc_node` VALUES (3, '新增员工', 'system', NULL, 'user/add', NULL, 2, NULL, 1, 0, '2021-12-08 15:57:23', NULL);
INSERT INTO `fc_node` VALUES (4, '编辑员工', 'system', NULL, 'user/edit', NULL, 2, NULL, 1, 0, '2021-12-08 15:57:49', NULL);
INSERT INTO `fc_node` VALUES (5, '删除员工', 'system', NULL, 'user/del', NULL, 2, NULL, 1, 0, '2021-12-08 15:58:09', NULL);
INSERT INTO `fc_node` VALUES (6, '角色管理', 'system', '/role/index', 'role/index', 'role', 1, '', 2, 0, '2021-12-08 15:59:20', NULL);
INSERT INTO `fc_node` VALUES (7, '添加角色', 'system', NULL, 'role/add', NULL, 6, NULL, 1, 0, '2021-12-08 16:01:29', NULL);
INSERT INTO `fc_node` VALUES (8, '编辑角色', 'system', NULL, 'role/edit', NULL, 6, NULL, 1, 0, '2021-12-08 16:01:33', NULL);
INSERT INTO `fc_node` VALUES (9, '删除角色', 'system', NULL, 'role/del', NULL, 6, NULL, 1, 0, '2021-12-08 16:01:37', NULL);
INSERT INTO `fc_node` VALUES (10, '部门管理', 'system', '/dept/index', 'dept/index', 'dept', 1, '', 2, 0, '2021-12-08 16:05:54', NULL);
INSERT INTO `fc_node` VALUES (11, '新增部门', 'system', NULL, 'dept/add', NULL, 10, NULL, 1, 0, '2021-12-08 16:06:18', NULL);
INSERT INTO `fc_node` VALUES (12, '编辑部门', 'system', NULL, 'dept/edit', NULL, 10, NULL, 1, 0, '2021-12-08 16:06:40', NULL);
INSERT INTO `fc_node` VALUES (13, '删除部门', 'system', NULL, 'dept/del', NULL, 10, NULL, 1, 0, '2021-12-08 16:06:58', NULL);
INSERT INTO `fc_node` VALUES (14, '字典配置', 'system', '/dict/index', 'dict/index', 'dict', 1, '', 2, 0, '2021-12-12 10:11:11', NULL);
INSERT INTO `fc_node` VALUES (15, '字典分类树', 'system', '/dictcate', 'dictcate/index', 'dictcate', 14, NULL, 1, 0, '2021-12-12 10:12:17', NULL);
INSERT INTO `fc_node` VALUES (16, '添加字典分类', 'system', NULL, 'dictcate/add', NULL, 15, NULL, 1, 0, '2021-12-12 10:13:04', NULL);
INSERT INTO `fc_node` VALUES (17, '编辑字典分类', 'system', NULL, 'dictcate/edit', NULL, 15, NULL, 1, 0, '2021-12-12 10:13:24', NULL);
INSERT INTO `fc_node` VALUES (18, '删除字典分类', 'system', NULL, 'dictcate/del', NULL, 15, NULL, 1, 0, '2021-12-12 10:13:45', NULL);
INSERT INTO `fc_node` VALUES (19, '添加字典项', 'system', NULL, 'dict/add', NULL, 14, NULL, 1, 0, '2021-12-12 10:15:30', NULL);
INSERT INTO `fc_node` VALUES (20, '编辑字典项', 'system', NULL, 'dict/edit', NULL, 14, NULL, 1, 0, '2021-12-12 10:15:26', NULL);
INSERT INTO `fc_node` VALUES (21, '删除字典项', 'system', NULL, 'dict/del', NULL, 14, NULL, 1, 0, '2021-12-12 10:15:50', NULL);
INSERT INTO `fc_node` VALUES (22, '低代码开发', 'system', '/online', '#', '#', 0, 'el-icon-cloudy', 2, 40, '2021-12-13 17:00:37', NULL);
INSERT INTO `fc_node` VALUES (23, '表单设计', 'system', '/form/index', 'form/index', 'form', 22, '', 2, 40, '2021-12-13 17:01:11', NULL);
INSERT INTO `fc_node` VALUES (24, '新增表单', 'system', NULL, 'form/add', NULL, 22, NULL, 1, 40, '2022-01-06 22:16:18', NULL);
INSERT INTO `fc_node` VALUES (25, '编辑表单', 'system', NULL, 'form/edit', NULL, 22, NULL, 1, 40, '2022-01-06 22:16:43', NULL);
INSERT INTO `fc_node` VALUES (26, '删除表单', 'system', NULL, 'form/del', NULL, 22, NULL, 1, 40, '2022-01-06 22:17:01', NULL);
INSERT INTO `fc_node` VALUES (27, '部署表单', 'system', NULL, 'form/deploy', NULL, 22, NULL, 1, 40, '2022-01-10 11:07:51', NULL);
INSERT INTO `fc_node` VALUES (28, '卸载表单', 'system', NULL, 'form/undeploy', NULL, 22, NULL, 1, 40, '2022-01-19 13:33:09', NULL);
INSERT INTO `fc_node` VALUES (58, '流程管理', 'system', '/flow', '#', '#', 0, 'el-icon-share', 2, 39, '2022-04-13 21:55:30', NULL);
INSERT INTO `fc_node` VALUES (59, '流程设计', 'system', '/flow/index', 'flow/index', 'flow', 58, NULL, 2, 39, '2022-04-13 21:56:35', NULL);
INSERT INTO `fc_node` VALUES (60, '新增流程', 'system', '/flow/add', 'flow/add', '', 59, NULL, 1, 39, '2022-04-13 21:57:15', NULL);
INSERT INTO `fc_node` VALUES (61, '编辑流程', 'system', '/flow/edit', 'flow/edit', NULL, 59, NULL, 1, 39, '2022-04-13 21:57:52', NULL);
INSERT INTO `fc_node` VALUES (63, '发布流程', 'system', '/flow/deploy', 'flow/deploy', NULL, 59, NULL, 1, 39, '2022-04-23 15:54:38', NULL);
INSERT INTO `fc_node` VALUES (64, '卸载流程', 'system', '/flow/undeploy', 'flow/undeploy', NULL, 59, NULL, 1, 39, '2022-04-23 15:55:28', NULL);
INSERT INTO `fc_node` VALUES (71, '报销申请', 'diy', '/design/baoxiao', '/curd/index?id=2', 'diy_2', 0, 'el-icon-cloudy', 2, 0, '2022-04-23 14:46:15', NULL);
INSERT INTO `fc_node` VALUES (72, '新增', 'diy', '', '/curd/add?id=2', '', 71, '', 1, 0, '2022-04-23 14:46:15', NULL);
INSERT INTO `fc_node` VALUES (73, '编辑', 'diy', '', '/curd/edit?id=2', '', 71, '', 1, 0, '2022-04-23 14:46:15', NULL);
INSERT INTO `fc_node` VALUES (74, '删除', 'diy', '', '/curd/del?id=2', '', 71, '', 1, 0, '2022-04-23 14:46:15', NULL);

-- ----------------------------
-- Table structure for fc_party_resource
-- ----------------------------
DROP TABLE IF EXISTS `fc_party_resource`;
CREATE TABLE `fc_party_resource`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `cate_id` int NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `resource_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_id` int NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fc_party_resource
-- ----------------------------
INSERT INTO `fc_party_resource` VALUES (1, NULL, 'asdfasdf', 'asfasfd', 'http://asdfasfd', 1, '2022-06-10 11:52:04', '2022-06-10 11:52:04');
INSERT INTO `fc_party_resource` VALUES (2, NULL, 'asdfasdf', 'asfasfd', 'http://asdfasfd', 1, '2022-06-10 11:52:23', '2022-06-10 11:52:23');
INSERT INTO `fc_party_resource` VALUES (3, 222, 'asdfasdf', 'asfasfd', 'http://asdfasfd', 1, '2022-06-10 11:53:27', '2022-06-10 11:53:27');

-- ----------------------------
-- Table structure for fc_party_resource_cate
-- ----------------------------
DROP TABLE IF EXISTS `fc_party_resource_cate`;
CREATE TABLE `fc_party_resource_cate`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_id` int NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fc_party_resource_cate
-- ----------------------------
INSERT INTO `fc_party_resource_cate` VALUES (2, 'asf222', 1, '2022-06-10 11:03:46', '2022-06-10 11:03:46');

-- ----------------------------
-- Table structure for fc_role
-- ----------------------------
DROP TABLE IF EXISTS `fc_role`;
CREATE TABLE `fc_role`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `name` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色名称',
  `role_node` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '角色拥有的菜单节点',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态  1 有效 2 无效',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fc_role
-- ----------------------------
INSERT INTO `fc_role` VALUES (1, '超级管理员', '*', 1, '2021-12-06 16:21:14', NULL);
INSERT INTO `fc_role` VALUES (2, '客服2', '2,3,4,5,10,11,12,13,1', 1, '2021-12-08 22:22:10', '2021-12-09 13:41:38');
INSERT INTO `fc_role` VALUES (3, '新的角色', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21', 1, '2022-01-05 14:50:59', '2022-01-05 14:51:27');

-- ----------------------------
-- Table structure for fc_user
-- ----------------------------
DROP TABLE IF EXISTS `fc_user`;
CREATE TABLE `fc_user`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `account` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '登录名称',
  `nickname` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '昵称',
  `password` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '管理员密码',
  `salt` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '当前用户加密盐',
  `avatar` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '管理员头像',
  `role_id` int NULL DEFAULT 0 COMMENT '所属角色id',
  `last_login_ip` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '最近一次登录ip',
  `last_login_time` datetime NULL DEFAULT NULL COMMENT '最近一次登录时间',
  `last_login_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '最近一次登录设备',
  `status` tinyint NULL DEFAULT 1 COMMENT '1 正常 2 禁用',
  `create_time` datetime NULL DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `account`(`account`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fc_user
-- ----------------------------
INSERT INTO `fc_user` VALUES (1, 'admin', '管理员', '2cbcb726501a786c2e1d605f488ad5f4', '61add43e50a51', 'https://www.cizhua.com/static/images/touxiang.png', 1, '192.168.0.102', '2022-06-10 10:54:17', 'PostmanRuntime/7.29.0', 1, '2021-12-06 16:36:47', NULL);
INSERT INTO `fc_user` VALUES (4, 'nick', '李四', '02f4f3b543120f563a37bf8b3580abd4', '61b4b684affaa', 'https://www.cizhua.com/static/images/touxiang.png', 2, NULL, NULL, NULL, 1, '2021-12-11 22:32:36', '2021-12-12 17:20:59');
INSERT INTO `fc_user` VALUES (5, 'huang', '阿黄', '409c5701c397f7b09c80858f95a86645', '61b4beda9e8e0', 'https://www.cizhua.com/static/images/touxiang.png', 2, NULL, NULL, NULL, 1, '2021-12-11 22:35:38', '2021-12-11 23:08:44');
INSERT INTO `fc_user` VALUES (6, 'lisi', '李四', 'b89391d4e696342e672fa6535220b7da', '61b6a3ce4e76c', 'https://www.cizhua.com/static/images/touxiang.png', 2, NULL, NULL, NULL, 1, '2021-12-13 09:37:18', NULL);
INSERT INTO `fc_user` VALUES (7, 'iqeqw', 'dada', 'd6ed5028b8cb4df64d7fccffccae162f', '61d40bce0d106', 'https://www.cizhua.com/static/images/touxiang.png', 2, NULL, NULL, NULL, 1, '2022-01-04 16:56:46', NULL);
INSERT INTO `fc_user` VALUES (8, 'rerer', '去问问去', '4273b5da936604ef3969d17578ea5767', '61d40e11df311', 'https://www.cizhua.com/static/images/touxiang.png', 2, NULL, NULL, NULL, 1, '2022-01-04 17:06:25', NULL);

SET FOREIGN_KEY_CHECKS = 1;
