<?php
declare (strict_types = 1);

namespace app\middleware;

use app\logic\TokenLogic;
use think\Response;

class Token
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        //
        $token = $request->header("token");
        if (empty($token)){
            return response(dataReturn(-1, "请登录"),200, [], "json");
        }
        $request->user_id = TokenLogic::getUserID($token);
        if (empty($request->user_id)){
            return response(dataReturn(-1, "登录过期"),200, [], "json");
        }

        return $next($request);
    }
}
