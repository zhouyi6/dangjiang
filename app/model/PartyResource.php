<?php
/**
 * PartyResource.php
 * @author pingo
 * @date 2022/6/10 10:04
 */

namespace app\model;

class PartyResource extends BaseModel
{
    protected $name = "party_resource";
}