<?php
/**
 * PartyResourceService.php
 * @author pingo
 * @date 2022/6/10 10:09
 */

namespace app\service;

use app\model\PartyResource;

class PartyResourceService
{
    /**
     * 获取所有
     *
     * @return array
     * @author pingo
     * @date 2022/6/10 10:24
     */
    public function getAll()
    {
        try {
            $list = (new PartyResource())->order("id", "desc")->select();
            return dataReturn(0,"success", [ "list" => $list, "count" => count($list)]);
        }catch (\Throwable $exception){
            return dataReturn(-1, $exception->getMessage());
        }
    }

    /**
     * 分页数据
     *
     * @param $param
     * @return array
     * @author pingo
     * @date 2022/6/10 10:24
     */
    public function getPageList($param)
    {
        try {
            $PartyResource = (new PartyResource);
            $pageSize = $param['page_size'];
            $page = $param['page'];
            $where = [];
            //搜索字段
            if (empty($param["title"])){
                $where[] = ["title", "=", $param["title"]];
            }
            if (empty($param["cate_id"])){
                $where[] = ["cate_id", "=", $param["cate_id"]];
            }
            
            $total = $PartyResource->where($where)->count();
            $list = $PartyResource->where($where)->order("id", "desc")->page($page, $pageSize)->select();
            $return_data = [
                "list" => $list,
                "total" => $total,
                "page"  => $page,
                "page_size" => $pageSize,
            ];
            return dataReturn(0, "success", $return_data);
        }catch (\Throwable $exception){
            return dataReturn(-1, $exception->getMessage());
        }
    }

    /**
     * 添加
     * @param $param
     * @return array
     */
    public function add($param)
    {
        try {
            $PartyResource = new PartyResource();
            $field = [
                "title" => $param["title"],
                "cate_id" => $param["cate_id"],
                "user_id" => $param["user_id"],
                "description" => $param["description"],
                "resource_url" => $param["resource_url"],
            ];

            $PartyResource->save($field);
            return dataReturn(0, "success");
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

    }

    /**
     * 编辑
     * @param $param
     * @return array
     */
    public function edit($param)
    {

        try {
            $PartyResource = new PartyResource();
            $PartyResourceModel = $PartyResource->find($param['id']);
            if (empty($PartyResourceModel)) {
                return dataReturn(-2, '数据不存在');
            }
            $field = [
                "title" => $param["title"],
                "user_id" => $param["user_id"],
                "description" => $param["description"],
                "resource_url" => $param["resource_url"],
            ];
            $PartyResourceModel->where('id', $param['id'])->save($field);
            return dataReturn(0, 'success');
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
    }

    /**
     * 删除
     * @param $id
     * @return array
     */
    public function del($id)
    {
        try {
            $PartyResource = new PartyResource();
            $has = $PartyResource->where('id', $id)->find();
            if (empty($has)) {
                return dataReturn(-2, '该数据已删除');
            }
            $PartyResource->where('id', $id)->delete();
            return dataReturn(0, 'success');
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

    }

    /**
     * 根据id获取信息
     * @param $cateId
     * @return array
     */
    public function getInfoById($id)
    {
        return  (new PartyResource())->getInfoById($id);
    }
}