<?php
/**
 * PartyResourceCateService.php
 * @author pingo
 * @date 2022/6/10 10:09
 */

namespace app\service;

use app\model\PartyResourceCate;
use think\facade\Log;

class PartyResourceCateService
{
    /**
     * 获取所有
     *
     * @return array
     * @author pingo
     * @date 2022/6/10 10:24
     */
    public function getAll()
    {
        try {
            $list = (new PartyResourceCate())->order("id", "desc")->select();
            return dataReturn(0,"success", [ "list" => $list, "count" => count($list)]);
        }catch (\Throwable $exception){
            return dataReturn(-1, $exception->getMessage());
        }
    }

    /**
     * 分页数据
     *
     * @param $param
     * @return array
     * @author pingo
     * @date 2022/6/10 10:24
     */
    public function getPageList($param)
    {
        try {
            $PartyResourceCate = (new PartyResourceCate);
            $pageSize = $param['page_size'];
            $page = $param['page'];
            $where = [];
            $total = $PartyResourceCate->where($where)->count();
            $list = $PartyResourceCate->where($where)->order("id", "desc")->page($page, $pageSize)->select();
            $return_data = [
                "list" => $list,
                "total" => $total,
                "page"  => $page,
                "page_size" => $pageSize,
            ];
            return dataReturn(0, "success", $return_data);
       }catch (\Throwable $exception){
            return dataReturn(-1, $exception->getMessage());
        }
    }

    /**
     * 添加
     * @param $param
     * @return array
     */
    public function add($param)
    {
        try {
            $PartyResourceCate = new PartyResourceCate();
            $has =   $PartyResourceCate->where('name', $param['name'])->find();
            if (!empty($has)) {
                return dataReturn(-2, '该分类已经存在');
            }
            $field = [
                "name" => $param["name"],
                "user_id" => $param["user_id"],
            ];
            $PartyResourceCate->save($field);
            return dataReturn(0, "success");
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

    }

    /**
     * 编辑
     * @param $param
     * @return array
     */
    public function edit($param)
    {

        try {
            $PartyResourceCate = new PartyResourceCate();
            $PartyResourceCateModel = $PartyResourceCate->find($param['id']);
            if (empty($PartyResourceCateModel)) {
                return dataReturn(-2, '数据不存在');
            }
            $field = [
                "name" => $param["name"],
                "user_id" => $param["user_id"],
            ];
            $PartyResourceCateModel->where('id', $param['id'])->save($field);
            return dataReturn(0, 'success');
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
    }

    /**
     * 删除
     * @param $id
     * @return array
     */
    public function del($id)
    {
        try {
            $PartyResourceCate = new PartyResourceCate();
            $has = $PartyResourceCate->where('id', $id)->find();
            if (empty($has)) {
                return dataReturn(-2, '该数据已删除');
            }
            $PartyResourceCate->where('id', $id)->delete();
            return dataReturn(0, 'success');
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

    }

    /**
     * 根据id获取信息
     * @param $cateId
     * @return array
     */
    public function getInfoById($id)
    {
        return  (new PartyResourceCate())->getInfoById($id);
    }
}