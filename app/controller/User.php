<?php

namespace app\controller;

use app\service\UserService;
use think\annotation\Inject;

class User extends Base
{
    /**
     * @Inject()
     * @var UserService
     */
    protected $userService;

    public function index()
    {
        $param = input('param.');

        $res = $this->userService->getUserList($param);

        return json(pageReturn($res));
    }

    public function add()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $res = $this->userService->addUser($param);
            return json($res);
        }
    }

    public function edit()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $res = $this->userService->editUser($param);
            return json($res);
        }
    }

    public function del()
    {
        $userId = input('param.id');

        $res = $this->userService->delUser($userId);
        return json($res);
    }
}