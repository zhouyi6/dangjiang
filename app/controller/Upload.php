<?php

namespace app\controller;

class Upload extends Base
{
    //上传图片
    public function doUpload()
    {
        $files = request()->file();
        try {
            if (empty($files)){
                    return jsonReturn(-1, "请选择文件，文件名称字段为：file");
             }
            validate(['file'=>'fileSize:1024000|fileExt:jpg,png,gif,doc,xls,xlsx,docx,pdf,ppt,pptx,zip'])
                ->check($files);
            $savename = [];
            foreach($files as $file) {
                $uri = \think\facade\Filesystem::disk("public")->putFile( 'topic', $file);
                $savename[] = env("APP_URL") . \think\facade\Filesystem::disk("public")->getConfig()->get("url") . "/" . $uri;
            }
            return jsonReturn(0, "success", $savename);
        } catch (\Throwable $e) {
            echo $e->getMessage();
        }

    }
}
