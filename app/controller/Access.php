<?php

namespace app\controller;

use app\BaseController;
use app\logic\TokenLogic;
use app\middleware\Token;
use app\service\LoginService;
use app\validate\UserValidate;
use think\exception\ValidateException;

class Access extends BaseController
{


//    public function initialize()
//    {
//        header("access-control-allow-headers: Accept,Authorization,Cache-Control,Content-Type,DNT,If-Modified-Since,Keep-Alive,Origin,User-Agent,X-Mx-ReqToken,X-Requested-With");
//        header("access-control-allow-methods: GET, POST, PUT, DELETE, HEAD, OPTIONS");
//        header("access-control-allow-credentials: true");
//        header("access-control-allow-origin: *");
//    }

    public function doLogin()
    {
        if (request()->isPost()) {

            $param = input('post.');
            try {

                validate(UserValidate::class)->scene('login')->check($param);
            } catch (ValidateException $e) {
                return json(dataReturn(-1, $e->getError()));
            }
            $loginService = new LoginService();
            $res = $loginService->login($param);

            return json($res);
        }
    }


    public function loginOut()
    {

        $token = $this->request->header("token");
        if ($token){
            TokenLogic::Remove($token);
        }
        return jsonReturn(0, 'success');
    }
}