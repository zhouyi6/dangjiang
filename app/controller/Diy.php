<?php
/**
 * Created by PhpStorm.
 * Date: 2022/4/2
 * Time: 22:26
 */

namespace app\controller;

use app\service\FormService;
use think\annotation\Inject;

class Diy extends Base
{
    /**
     * @Inject()
     * @var FormService
     */
    protected $formService;

    public function index()
    {
        return json($this->formService->getInfoById(input('param.')));
    }

    public function getForm()
    {
        return json($this->formService->getFormDataById(input('param.id')));
    }

    public function add()
    {
        return json($this->formService->addDiyData(input('post.')));
    }

    public function edit()
    {
        return json($this->formService->editDiyData(input('post.')));
    }

    public function getInfo()
    {
        return json($this->formService->getDiyDataInfo(input('param.')));
    }

    public function del()
    {
        return json($this->formService->delTableData(input('param.')));
    }
}