<?php

namespace app\validate;

use think\Validate;

class UserValidate extends Validate
{
    protected $rule = [
        'account|登录名' => 'require|max:55|alpha',
        'password|密码' => 'require',
        'avatar|头像' => 'require',
        'nickname|员工名' => 'require',
        'role_id|所属角色' => 'require',
        'password|登录密码' => 'require',
        'status|状态' => 'require',
        'is_leader|是否主管' => 'require'
    ];

    protected $scene = [
        'login' => ['account', 'password'],
        'edit' =>  ['account', 'avatar', 'nickname', 'role_id', 'status', 'is_leader']
    ];
}