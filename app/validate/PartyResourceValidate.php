<?php
/**
 * PartyResourceValidate.php
 * @author pingo
 * @date 2022/6/10 10:25
 */

namespace app\validate;

class PartyResourceValidate extends \think\Validate
{
    protected $rule = [
        'id|ID' => 'require',
        "title" => "require",
        "description" => "require",
        "resource_url" => "require",
        "cate_id" => "require"
    ];

    protected $scene = [
        'add' => ['title', "description", "resource_url", "cate_id"],
        'edit' =>  ['id',"description", "resource_url", "cate_id"],
        'del' =>  ['id',]
    ];
}