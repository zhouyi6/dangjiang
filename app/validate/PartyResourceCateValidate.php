<?php
/**
 * PartyResourceCateValidate.php
 * @author pingo
 * @date 2022/6/10 10:25
 */

namespace app\validate;

class PartyResourceCateValidate extends \think\Validate
{
    protected $rule = [
        'name|名称' => 'require|max:100',
        'id|ID' => 'require',
        "page|页码" => "require",
        "page_size|分页记录条" => "require",

    ];

    protected $scene = [
        'add' => ['name', ],
        'edit' =>  ['id', 'name',],
        'del' =>  ['id',],
        "list" => ["page", "page_size"],
    ];


}